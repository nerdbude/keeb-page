<!--
.. title: Medien
.. slug: medien
.. date: 2021-10-07 19:20:00 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: Eine Linksammlung zu unterschiedlichen Medien rund um Mechanische Tastaturen.
.. type: text
-->

# Blogs

- [Keyboard Builders' Digest](https://kbd.news/) (Englisch) ist ein kuratierter Newsletter, der sich mit dem Selbstbau von mechanischen Tastaturen beschäftigt.
- [GOLEM keyboard project](https://golem.hu/) (Englisch) bietet zahlreiche Informationen zum Eigenbau von mechanischen Tastaturen und legt dabei seinen Fokus auf _Split Keyboards_.
- [ThereminGoat's Switches](https://www.theremingoat.com/) (Englisch) ThereminGoat hat wahrscheinlich die größte Switch-Sammlung überhaupt. Auf dem Blog werden einzelne Switches bis ins kleinste Detail analysiert und bewertet. 

# Magazine

- [Damn Fine Keyboards](https://www.damnfinekeyboards.com/) ist ein unregelmäßig erscheinendes Magazin, welches Bilder schöner Tastaturen zeigt.

# YouTube-Channel

- [Thomas' Keyboard Reviews (Chyrosran22, englisch)](https://www.youtube.com/user/Chyrosran22)

# Podcasts

- [Click! Clack! Hack! (deutsch)](http://clickclackhack.de/) ist ein deutschsprachiger Podcast rund um Hackbretter von Vintage bis modern, von gekauft bis selbstgebaut.
- [The Thocc (englisch)](https://thethocc.libsyn.com/) Hier unterhält sich der Host Vogon  mit anderen Enthusiasten und redet über aktuelle Themen aber auch über ihre Präferenzen. 
- [The Board (englisch)](https://theboard.libsyn.com/) Bei "The Board" spricht Don weniger über News, Groupbuys oder Interestchecks, sondern mehr über die Philosophie hinter den Keyboards.

