<!--
.. title: Communities
.. slug: communities
.. date: 2021-10-06 22:13:56 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Du willst dich über mechanische Tastaturen austauschen? Dann hier entlang!

# Chat

Die vorherrschende Plattform für den schnellen Austausch ist _Discord_.

- [Mechanische Tastaturen Deutschland](https://discord.gg/AnKuj8Y) ist ein deutschsprachiger Discord-Server. Es wird kann sich über quasi alle Themen ausgetauscht werden, außerdem gibt es dedizierte Bereiche für Händler, Treffen und auf diesen Server beschränkte _Group Buys_.

# Forum

Foren eignen sich prima für den Austausch, wenn es keiner sofortigen Antwort bedarf oder die Texte länger werden (sollen).

- [Keebtalk](https://www.keebtalk.com/) (Englisch) ist ein junges Forum, welches großen Wert auf einen freundlichen Umgang, Wissenaustausch und dem Schaffen besserer Hardware legt.
- [geekhack](https://geekhack.org/) (Englisch) ist eines der ältesten Foren zu mechanischen Tastaturen. Es wird viel Wert auf Diskussionen gelegt und hier finden sich zahlreiche _ICs_ und _GBs_. Es gibt eine große Themenvielfalt.

## Foren mit Bild-Fokus

- [keebs.gg](https://keebs.gg/) ist eine Community, die sich vorranging um das Präsentieren eigener _Builds_ dreht.
